#!/bin/sh

### .config ###
cp /home/$USER/.config/compton/* ./home/metr1xx/.config/compton/
cp /home/$USER/.config/i3/* ./home/metr1xx/.config/i3/
cp /home/$USER/.config/openbox/* ./home/metr1xx/.config/openbox/
cp /home/$USER/.config/polybar/* ./home/metr1xx/.config/polybar/
cp /home/$USER/.config/rofi/* ./home/metr1xx/.config/rofi/

### screen layout files ###
cp /home/$USER/.screenlayout/* ./home/metr1xx/.screenlayout/

### ranger stuff plus wallpaper ###
cp /home/$USER/.config/ranger/rifle.conf ./home/metr1xx/.config/ranger/
cp /home/$USER/.config/ranger/rc.conf ./home/metr1xx/.config/ranger/
cp /home/$USER/.config/ranger/wall.png ./home/metr1xx/.config/ranger/

### scripts ###
cp -r /home/$USER/Scripts/* ./home/metr1xx/Scripts/

### music stuffs ###
cp /home/$USER/.mpd/mpd.conf ./home/metr1xx/.mpd/
cp /home/$USER/.ncmpcpp/* ./home/metr1xx/.ncmpcpp/

### vim config ###
cp /home/$USER/.vim/init.vim ./home/metr1xx/.vim/
cp /home/$USER/.vim/autoload/plug.vim ./home/metr1xx/.vim/autoload/plug.vim

### shell/terminal configs ###
cp /home/$USER/.Xdefaults ./home/metr1xx/.Xdefaults
cp /home/$USER/.config/kitty/* ./home/metr1xx/.config/kitty/
cp /home/$USER/.zshrc ./home/metr1xx/.zshrc
