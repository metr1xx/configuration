!/bin/sh

# run the network and bluetooth applet
nm-applet &
blueman-applet &

# start polkit for root authentication
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# start compton compositor
compton -f --blur-method kawase --blur-strength 10 --config ~/.config/compton/compton.conf &

# program that makes windows flash when focused
#flashfocus &

# start mpd
[ ! -s ~/.config/mpd/pid ] && mpd

# try to switch to desktop screen
#sh ~/.screenlayout/desktopscreenonly.sh &
sh ~/Scripts/checkforscreens.sh

# start polybar
sh ~/Scripts/polybarlaunch.sh &

# wait for the screen layout to finish
sleep 3

# set background image
feh --bg-fill ~/.config/ranger/wall.png
