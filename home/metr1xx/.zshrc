# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=/home/mtrx/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="lambda"
POWERLINE_DISABLE_RPROMPT="true"
POWERLINE_PATH="short"
POWERLINE_NO_BLANK_LINE="true"
# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
#
################################################################
#
# AUTOLOGIN
#
################################################################

if [[ -z $DISPLAY ]] && (( $EUID != 0 )) {
    [[ ${TTY/tty} != $TTY ]] && (( ${TTY:8:1} <= 3 )) &&
        startx &
}

################################################################
#
# CUSTOM BINDS
#
################################################################

#echo "Hello, $USER!"
#echo "`date +'%A %F'`"
datetime=$(date '+%F_%H-%M-%S')
#echo "`date +'%r'`"

alias syu='sudo pacman -Syu'
alias syyu='sudo pacman -Syyu'
alias pacs='sudo pacman -S'
alias pacr='sudo pacman -R'
alias neo='neofetch'
alias ei3='nvim ~/.config/i3/config'
alias epb='nvim ~/.config/polybar/config'
alias eob='nvim ~/.config/openbox/rc.xml'
alias eur='nvim ~/.Xdefaults'
alias ebash='nvim ~/.bashrc'
alias ezsh='nvim ~/.zshrc'
alias screenie="maim ~/Pictures/Screenshots/'$datetime'.png && echo 'Screenshot saved at ~/Pictures/Screenshots/'$datetime'.png'"
alias off='poweroff'
alias rb='reboot'
alias wp='cd ~/hdd/media/wallpapers && ranger'
alias lock='i3lock-next "$USER" "Roboto" 20'
alias music='ncmpcpp'
alias clock='ncmpcpp -s clock'
alias visualizer='ncmpcpp -s visualizer'
alias v='nvim'
alias vim='nvim'
alias httpserver='python -m http.server'
alias npmserver='npm start'
alias l="ls -l"
alias al="ls -al"
alias :q="exit"
alias :wq="exit"
alias emacs="emacs -nw"
alias logout="pkill -KILL -u $USER"
alias feh="feh -Z -B black"

alias arise="pactl set-sink-volume 0 +5% && pactl set-sink-volume 1 +5%"
alias alower="pactl set-sink-volume 0 -5% && pactl set-sink-volume 1 -5%"

export EDITOR="nvim"
export BROWSER="chromium"
export TERM="xterm-256color"
export TZ='Europe/Berlin'
