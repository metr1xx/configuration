" jump to last open file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
set title
" display line numbers
set number
" replace tabs with spaces
set expandtab
" tell vim how many colums vim uses when you hit tab 
set tabstop=4
" insert <amount> of spaces for tabs
set shiftwidth=4
" enable syntax highlighting
syntax on
" enable soft wrapping of textblocks
set wrap linebreak
" share vim and system clipboard
if has('unnamedplus')
    set clipboard=unnamed,unnamedplus
endif

let s:vim_plug = '~/.local/share/nvim/site/autoload/plug.vim'
"if we dont have vimplug yet use this to disable erring first run sections
let s:first_run = 0
if empty(glob(s:vim_plug, 1))
    let s:first_run = 1
    execute 'silent !curl -fLo' s:vim_plug '--create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endif

call plug#begin('~/.local/share/nvim/plugged')

" Plug this in if you want to update!
" Plug 'isaacmorneau/vim-update-daily'

" little helpers
Plug 'tpope/vim-fugitive'
Plug 'benekastah/neomake'
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'

" coloring / highlighting
Plug 'ntpeters/vim-better-whitespace'
Plug 'lilydjwg/colorizer'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" workflow
Plug 'aurieh/discord.nvim', { 'do': ':UpdateRemotePlugins'}
Plug 'junegunn/goyo.vim'

" language plugins
Plug 'mattn/emmet-vim'
Plug 'luochen1990/rainbow' " rainbow highlight brackets

" deoplete syntax completion
Plug 'Shougo/deoplete.nvim'
let g:deoplete#enable_at_startup = 1
" tab completion
inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ deoplete#mappings#manual_complete()
function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" deoplete for C/C++
Plug 'zchee/deoplete-clang'
let g:deoplete#sources#clang#libclang_path = '/usr/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/lib/clang'

" deoplete for python
Plug 'zchee/deoplete-jedi'
let g:deoplete#sources#jedi#statement_length = 50
" enable this for type information
let g:deoplete#sources#jedi#enable_typeinfo = 0

" deoplete for JS
Plug 'carlitux/deoplete-ternjs'
let g:deoplete#sources#ternjs#tern_bin = '/usr/bin/tern'
let g:deoplete#sources#ternjs#types = 1

" deoplete for rust
Plug 'racer-rust/vim-racer'
set hidden
let g:racer_cmd = "/home/mtrx/.cargo/bin/racer"

call plug#end()

"rainbow parenthesis config
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
let g:rainbow_conf = {
	\	'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick'],
	\	'ctermfgs': ['lightblue', 'lightyellow', 'lightcyan', 'lightmagenta'],
	\	'operators': '_,_',
	\	'parentheses': ['start=/(/ end=/)/ fold', 'start=/\[/ end=/\]/ fold', 'start=/{/ end=/}/ fold'],
	\	'separately': {
	\		'*': {},
	\		'tex': {
	\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/'],
	\		},
	\		'lisp': {
	\			'guifgs': ['royalblue3', 'darkorange3', 'seagreen3', 'firebrick', 'darkorchid3'],
	\		},
	\		'vim': {
	\			'parentheses': ['start=/(/ end=/)/', 'start=/\[/ end=/\]/', 'start=/{/ end=/}/ fold', 'start=/(/ end=/)/ containedin=vimFuncBody', 'start=/\[/ end=/\]/ containedin=vimFuncBody', 'start=/{/ end=/}/ fold containedin=vimFuncBody'],
	\		},
	\		'html': {
	\			'parentheses': ['start=/\v\<((area|base|br|col|embed|hr|img|input|keygen|link|menuitem|meta|param|source|track|wbr)[ >])@!\z([-_:a-zA-Z0-9]+)(\s+[-_:a-zA-Z0-9]+(\=("[^"]*"|'."'".'[^'."'".']*'."'".'|[^ '."'".'"><=`]*))?)*\>/ end=#</\z1># fold'],
	\		},
	\		'css': 0,
	\	}
	\}

let g:airline_powerline_fonts = 1
let g:airline_theme='base16_ashes'
let g:Powerline_symbols = 'fancy'

set laststatus=2
set noshowmode

" Toggle Nerd Tree
map <C-n> :NERDTreeToggle<CR>
" Compile current file to pdf using pandoc
nnoremap <C-p> :!pandoc -V geometry:"top=2.5cm, bottom=1.5cm, left=3cm, right=3cm" %:p -o %:p:r.pdf<enter>
" Source init.vim
nnoremap <C-s> :source ~/.vim/init.vim<enter>
" Run make in the current files directory
nnoremap <C-m> :!make %:p:h<enter>

"""""""""""""
" COLORSCHEME
"""""""""""""

"set colorcolumn=80
"colorscheme delek
highlight Search ctermfg=black ctermbg=red
highlight ColorColumn ctermfg=black ctermbg=darkgray
highlight LineNr ctermfg=black ctermfg=red

