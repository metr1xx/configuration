#!/bin/sh

### .config ###
cp ./home/metr1xx/.config/compton/* /home/$USER/.config/compton/*
cp ./home/metr1xx/.config/i3/* /home/$USER/.config/i3/*
cp ./home/metr1xx/.config/openbox/* /home/$USER/.config/openbox/*
cp ./home/metr1xx/.config/polybar/* /home/$USER/.config/polybar/*
cp -r ./home/metr1xx/.config/ranger/* /home/$USER/.config/ranger/*
cp ./home/metr1xx/.config/rofi/* /home/$USER/.config/rofi/*
cp ./home/metr1xx/.screenlayout/* /home/$USER/.screenlayout/*

### scripts ###
cp ./home/metr1xx/Scripts/* /home/$USER/Scripts/*

### music stuffs ###
cp -r ./home/metr1xx/.mpd/* /home/$USER/.mpd/*
cp ./home/metr1xx/.ncmpcpp/* /home/$USER/.ncmpcpp/*

### vim config ###
cp ./home/metr1xx/.vim/* /home/$USER/.vim/init.vim
cp ./home/metr1xx/.vim/autoload/plug.vim /home/$USER/.vim/autoload/plug.vim
ln -sf /home/$USER/.vim/ /home/$USER/.config/nvim/

### shell/terminal configs ###
cp ./home/metr1xx/.Xdefaults /home/$USER/.Xdefaults
cp ./home/metr1xx/.zshrc /home/$USER/.zshrc
cp ./home/metr1xx/.config/kitty/* /home/$USER/.config/kitty/*
