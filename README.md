# My dotfiles and other important configurations.
Featuring nifty stuff like Oh-My-Zsh, Polybar, Rofi, a heavily modified NeoVim and Ranger.

![Screenshot](https://raw.githubusercontent.com/metr1xx/rice/master/screenshots/ss.png)

# Some information:

How to set wallpapers the good way:
    1) select the image you want to use as a background in ranger
    2) press 'b', then 'w'

My .zshrc features a lot of binds. Read them, they may be useful!

You may want to set new paths to your music folder in ncmpcpp/mpd, otherwise it probably won't work for you.

To install the config, just install all the necessary programs first, then run "install.sh"

To update the config after you made changes, just run updateconfig.sh. This script can also be used to make your config portable and put it on a usb stick, for example.

# Things you should install:

- alsa
- feh
- i3-gaps
- kitty
- mpd
- mupdf
- ncmpcpp
- oh-my-zsh
- pulseaudio
- ranger
- rofi
- zsh
- i3-lock-color-git [AUR]
- polybar [AUR]
- python-neovim [AUR]
- vim-airline [AUR]
- vim-airline-themes [AUR]

## fonts
- ttf-hack
- uw-ttyp0-font [AUR]
- nerd-fonts-complete [AUR]
